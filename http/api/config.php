<?php
// +----------------------------------------------------------------------
// | INPHP
// | Copyright (c) 2023 https://inphp.cc All rights reserved.
// | Author: 幺月儿(https://gitee.com/lulanyin) Email: inphp@qq.com
// | 该文件的开源协议以所在项目的LICENSE文件为准，请遵守开源协议要求
// +----------------------------------------------------------------------
// | 配置
// +----------------------------------------------------------------------
namespace app\finance\http\api;

use app\admin\attributes\auth;
use Inphp\Core\Object\Message;
use Inphp\Core\Util\Str;

#[auth] class config
{
    public function index(): Message
    {
        $name = GET("name");
        $name = !empty($name) ? Str::trim($name) : null;
        if (empty($name) || !in_array($name, ["balance", "currency", "orderTypes"])) {
            return httpMessage("无效参数");
        }
    }

    public function save(): Message
    {

    }
}