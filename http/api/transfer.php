<?php
// +----------------------------------------------------------------------
// | INPHP
// | Copyright (c) 2023 https://inphp.cc All rights reserved.
// | Author: 幺月儿(https://gitee.com/lulanyin) Email: inphp@qq.com
// | 该文件的开源协议以所在项目的LICENSE文件为准，请遵守开源协议要求
// +----------------------------------------------------------------------
// | 账户资产转账
// +----------------------------------------------------------------------
namespace app\finance\http\api;

use app\admin\attributes\auth;

#[auth] class transfer
{
    /**
     * 账户UID
     * @var int
     */
    public int $uid = 0;

    /**
     * 账户数据
     * @var array
     */
    public array $user = [];
}