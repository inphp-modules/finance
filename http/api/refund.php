<?php
// +----------------------------------------------------------------------
// | INPHP
// | Copyright (c) 2023 https://inphp.cc All rights reserved.
// | Author: 幺月儿(https://gitee.com/lulanyin) Email: inphp@qq.com
// | 该文件的开源协议以所在项目的LICENSE文件为准，请遵守开源协议要求
// +----------------------------------------------------------------------
// | 退款接口
// +----------------------------------------------------------------------
namespace app\finance\http\api;

use app\admin\attributes\auth;
use app\finance\model\CashierModel;
use app\finance\model\RefundModel;
use Inphp\Core\Db\Db;
use Inphp\Core\Db\PDO\Query;
use Inphp\Core\Modules;
use Inphp\Core\Object\Message;
use Inphp\Core\Util\Str;

class refund
{
    /**
     * UID
     * @var int
     */
    public int $uid = 0;

    /**
     * USER
     * @var array
     */
    public array $user = [];

    /**
     * 获取指定ID的退款订单
     * @return Message
     */
    #[auth] public function index(): Message
    {
        $id = GET("id");
        $id = is_numeric($id) && $id > 0 ? $id : 0;
        if ($id <= 0) {
            return httpMessage("ID参数无效");
        }
        $admin = GET("admin");
        $admin = $admin == 1 ? 1 : 0;
        $sso = Modules::getModule("sso");
        //判断是不是管理员
        $admin = $admin == 1 && ($this->user["admin"] == 1 || ($sso && !empty($this->user["groups"]) && \app\sso\model\UserModel::matchPermission(["finance/cashier/refundList"], $this->user["groups"])));
        $db = RefundModel::emptyQuery("r")
            ->join(CashierModel::tableName()." c", "r.cashierId=c.id")
            ->select([
                "r.*",
                "c.orderId, c.orderType, c.amount, c.currency, c.info, c.payment, c.payedAmount, c.payedTradeNo, c.paymentAppId"
            ]);
        if ($admin && $sso) {
            $db->leftJoin(\app\sso\model\UserModel::tableName()." u", "r.uid=u.uid")
                ->addSelect([
                    "u.username, u.nickname, u.countryCode, u.phone"
                ]);
        }
        if (!$admin) {
            $db->where("r.uid", $this->uid);
        }
        $item = $db->where("r.id", $id)->first();
        if (empty($item)) {
            return httpMessage("未找到订单数据");
        }
        $payments = CashierModel::payments();
        if ($sso && $admin) {
            $item['nickname'] = \app\sso\model\UserModel::decodeNickname($item['nickname']);
        }
        $item["orderName"] = CashierModel::orderName($item["orderType"]);
        $item["paymentName"] = $payments[$item["payment"]] ?? "未知";
        //返回数据
        return httpMessage($item);
    }

    /**
     * 列表
     * 普通用户可以查看自己的列表
     * 管理员可查看全部
     * @return Message
     */
    #[auth] public function list(): Message
    {
        $admin = GET("admin");
        $admin = $admin == 1 ? 1 : 0;
        $sso = Modules::getModule("sso");
        $admin = $admin == 1 && ($this->user["admin"] == 1 || ($sso && !empty($this->user["groups"]) && \app\sso\model\UserModel::matchPermission(["finance/cashier/refundList"], $this->user["groups"])));
        $db = RefundModel::emptyQuery("r")
            ->join(CashierModel::tableName()." c", "r.cashierId=c.id")
            ->select([
                "r.*",
                "c.orderId, c.orderType, c.currency, c.payment as cashierPayment, c.info, c.payedTradeNo, c.paymentAppId"
            ]);
        if ($admin && $sso) {
            $db->leftJoin(\app\sso\model\UserModel::tableName()." u", "r.uid=u.uid")
                ->addSelect([
                    "u.nickname, u.username, u.countryCode, u.phone"
                ]);
        }
        if (!$admin) {
            $db->where("r.uid", $this->uid);
        }
        //搜索
        $keyword = GET("keyword");
        $keyword = !empty($keyword) ? Str::trim($keyword) : null;
        if (!empty($keyword)) {
            if ($sso && $admin == 1) {
                if (is_numeric($keyword) && strlen($keyword) < 12) {
                    //数字，查询手机号码
                    $db->where(function (Query $q) use ($keyword) {
                        $q->whereLike("u.phone", $keyword)
                            ->orWhereLike("c.orderId", $keyword)
                            ->orWhereLike("c.id", $keyword)
                            ->orWhereLike("r.id", $keyword);
                    });
                } else {
                    //其它，查询昵称
                    $db->where(function(Query $q) use($keyword) {
                        $q->whereLike("u.username", $keyword)
                            ->orWhereLike("from_base64(substr(u.nickname, 23))", $keyword);
                    });
                }
            } else {
                $db->where(function(Query $q) use($keyword) {
                    $q->whereLike("c.orderId", $keyword)->orWhereLike("r.id", $keyword)->orWhereLike("c.id", $keyword);
                });
            }
        }
        //
        $state = GET("state");
        $state = $state == 1 || $state == 2 || $state == '0' ? $state : null;
        if (!is_null($state)) {
            $db->where("r.state", $state);
        }
        //支付方式、退款方式
        $payment = GET("payment");
        $payment = !empty($payment) ? Str::trim($payment) : null;
        $payments = CashierModel::payments();
        if (!empty($payment) && in_array($payment, array_keys($payments))) {
            $db->where("c.payment", $payment);
        }
        //行数
        $rows = $db->rows();
        //当前页数
        $page = GET('page', 1);
        $page = is_numeric($page) && $page > 0 ? ceil($page) : 1;
        //总页数
        $pages = 1;
        //列表数据
        $list = [];
        if ($rows > 0) {
            $total = GET("total", 30);
            $total = is_numeric($total) && $total > 2 ? $total : 30;
            $pages = ceil($rows / $total);
            $page = $page > $pages ? $pages : $page;
            $offset = $total * ($page - 1);
            $list = $db->orderBy("r.time", "desc")->get($total, $offset);
            foreach ($list as &$item)
            {
                if ($sso && $admin) {
                    $item['nickname'] = \app\sso\model\UserModel::decodeNickname($item['nickname']);
                }
                $item["orderName"] = CashierModel::orderName($item["orderType"]);
                $item["cashierPaymentName"] = CashierModel::getPayment($item["cashierPayment"]);
                $item["refundPaymentName"] = CashierModel::getPayment($item["payment"]);
                $item["currencyUnit"] = CashierModel::currencyUnit($item["currency"]);
            }
        }
        return httpMessage(compact("rows", "page", "pages", "list"));
    }

    /**
     * 退款操作确认，可拒绝
     * @return Message
     */
    #[auth("finance/cashier/refundConfirm")] public function confirm(): Message
    {
        //最终结果
        $state = POST("state");
        $state = $state == 1 || $state == '0' ? $state : null;
        if (is_null($state)) {
            return httpMessage("state参数无效");
        }
        $id = POST("id");
        $id = is_numeric($id) && $id > 0 ? ceil($id) : 0;
        if ($id <= 0) {
            return httpMessage("ID参数无效");
        }
        //获取数据
        $data = RefundModel::emptyQuery("r")
            ->join(CashierModel::tableName()." c", "r.cashierId=c.id")
            ->where("r.id", $id)
            ->select([
                "r.*",
                "c.payedAmount, c.payedTradeNo, c.paymentAppId, c.orderType, c.orderId"
            ])->first();
        if (empty($data)) {
            return httpMessage("未找到退单订单");
        }
        //状态判断
        if ($data["state"] == 1 || $data["state"] == 0) {
            return httpMessage("退款单已无效，不可操作");
        }
        if ($state == 1) {
            //确认通过
            if (!RefundModel::confirm($data)) {
                return httpMessage("退款处理失败，请稍后再试");
            }
            return httpMessage(0, "退款处理成功");
        } else {
            //关闭订单，需要退回数额
            Db::begin();
            $db = RefundModel::emptyQuery()
                ->where("id", $id)
                ->set(["state" => 0]);
            if (!$db->update()) {
                Db::rollback();
                return httpMessage("关闭订单失败");
            }
            //退回数额
            /*
            $db2 = CashierModel::emptyQuery()
                ->where("id", $data["cashierId"])
                ->decrement("refundAmount", $data["amount"]);
            if (!$db2->update()) {
                Db::rollback();
                return httpMessage("回退退款金额失败");
            }*/
            Db::commit();
            return httpMessage(0, "退款订单已关闭");
        }
    }
}