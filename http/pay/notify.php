<?php
// +----------------------------------------------------------------------
// | INPHP
// | Copyright (c) 2023 https://inphp.cc All rights reserved.
// | Author: 幺月儿(https://gitee.com/lulanyin) Email: inphp@qq.com
// | 该文件的开源协议以所在项目的LICENSE文件为准，请遵守开源协议要求
// +----------------------------------------------------------------------
// | 支付成功异步通知
// +----------------------------------------------------------------------
namespace app\finance\http\pay;

class notify
{
    //统一方法
    public function __call(string $name, array $arguments): mixed
    {
        list($name, $appId) = explode("_", $name);
        // TODO: Implement __call() method.
        $class = join("\\", ['app\finance\onlinePay', $name, 'pay']);
        if (!class_exists($class)) {
            return httpMessage("不存在该支付回调页面，请确认地址参数是否正确");
        }
        if (!method_exists($class, "notify")) {
            return httpMessage("不存在该支付回调方法，请确认地址参数是否正确");
        }
        //执行方法
        $obj = new $class();
        return $obj->notify($appId);
    }
}