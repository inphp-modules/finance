<?php
// +----------------------------------------------------------------------
// | INPHP
// | Copyright (c) 2023 https://inphp.cc All rights reserved.
// | Author: 幺月儿(https://gitee.com/lulanyin) Email: inphp@qq.com
// | 该文件的开源协议以所在项目的LICENSE文件为准，请遵守开源协议要求
// +----------------------------------------------------------------------
// | 支付完成返回页面
// +----------------------------------------------------------------------
namespace app\finance\http\pay;

class redirect
{
    //统一执行
    public function __call(string $name, array $arguments)
    {
        list($name, $appId) = explode("_", $name);
        $class = join("\\", ['app\finance\onlinePay', $name, 'pay']);
        if (!class_exists($class)) {
            showErrorPage("不存在该支付回调页面，请确认地址参数是否正确");
        }
        if (!method_exists($class, "callback")) {
            showErrorPage("不存在该支付回调方法，请确认地址参数是否正确");
        }
        //执行方法
        $obj = new $class();
        return $obj->callback($appId);
    }
}