DROP TABLE IF EXISTS `pre_finance_balance`;
CREATE TABLE `pre_finance_balance` (
  `uid` bigint NOT NULL COMMENT '用户UID',
  `balance` decimal(20,2) DEFAULT '0.00' COMMENT '余额',
  `integral` bigint DEFAULT '0' COMMENT '积分',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='会员资产余额';

DROP TABLE IF EXISTS `pre_finance_balanceHistory`;
CREATE TABLE `pre_finance_balanceHistory` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `uid` bigint unsigned NOT NULL COMMENT '用户ID',
  `name` varchar(30) COLLATE utf8mb4_general_ci NOT NULL COMMENT '资产类型名称',
  `amount` decimal(20,2) NOT NULL COMMENT '变动金额',
  `balance` decimal(20,2) NOT NULL COMMENT '变动后剩余的余额',
  `time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '变动时间',
  `code` varchar(100) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '变动指令',
  `log` text COLLATE utf8mb4_general_ci COMMENT '附加说明',
  `sign` char(32) COLLATE utf8mb4_general_ci NOT NULL COMMENT '加密签名数据',
  `editor` bigint DEFAULT '0' COMMENT '管理员UID',
  `linkId` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '关联ID',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `uid` (`uid`) USING BTREE,
  KEY `string` (`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='钱包流水';

DROP TABLE IF EXISTS `pre_finance_cashier`;
CREATE TABLE `pre_finance_cashier` (
  `id` bigint NOT NULL COMMENT '收银序号',
  `uid` bigint DEFAULT '0' COMMENT '账号UID',
  `orderId` text COLLATE utf8mb4_general_ci NOT NULL COMMENT '订单号',
  `orderType` varchar(30) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '订单类型',
  `amount` decimal(20,2) NOT NULL COMMENT '支付数额',
  `currency` varchar(30) COLLATE utf8mb4_general_ci DEFAULT 'CNY' COMMENT '计价货币',
  `currencyRatio` decimal(20,5) NOT NULL DEFAULT '1.00000' COMMENT '支付方式的汇率',
  `info` text COLLATE utf8mb4_general_ci COMMENT '订单信息',
  `time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '付款创建时间',
  `ip` varchar(128) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'IP地址',
  `payed` tinyint(1) DEFAULT '0' COMMENT '是否已支付',
  `payment` varchar(30) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '付款方式',
  `paymentAppId` varchar(64) COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '付款使用的第三方APPID',
  `payedTime` datetime DEFAULT NULL COMMENT '付款时间',
  `payedResult` text COLLATE utf8mb4_general_ci COMMENT '第三方平台返回原数据',
  `payedAmount` decimal(20,2) DEFAULT '0.00' COMMENT '已付款数额',
  `payedTradeNo` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '第三方付款成功交易号',
  `refund` tinyint(1) DEFAULT '0' COMMENT '是否已退款',
  `refundId` text COLLATE utf8mb4_general_ci COMMENT '退款编号 ',
  `refundTime` datetime DEFAULT NULL COMMENT '退款时间',
  `refundAmount` decimal(20,2) DEFAULT '0.00' COMMENT '退款数额',
  `editor` bigint DEFAULT '0' COMMENT '操作人UID',
  `bak` text COLLATE utf8mb4_general_ci COMMENT '备注',
  `notify` int DEFAULT '0' COMMENT '执行收银通知次数',
  `notified` int DEFAULT '0' COMMENT '接收到通知的次数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='统一收银台';

DROP TABLE IF EXISTS `pre_finance_checkout`;
CREATE TABLE `pre_finance_checkout` (
  `orderId` bigint unsigned NOT NULL COMMENT '订单号',
  `uid` bigint NOT NULL COMMENT '用户UID',
  `balance` varchar(50) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '提现资产类型',
  `quantity` decimal(20,2) NOT NULL COMMENT '数量',
  `currency` varchar(30) COLLATE utf8mb4_general_ci DEFAULT 'CNY' COMMENT '计价货币',
  `currencyRatio` decimal(20,2) DEFAULT '1.00' COMMENT '单价',
  `amount` decimal(20,2) NOT NULL COMMENT '总金额',
  `fee` decimal(20,2) DEFAULT '0.00' COMMENT '手续费',
  `feeTo` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '手续费划入',
  `getAmount` decimal(20,2) NOT NULL COMMENT '最终到账金额',
  `bank` varchar(100) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '收款方式',
  `name` varchar(30) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '账户名',
  `account` varchar(30) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '账户',
  `state` tinyint(1) DEFAULT '2' COMMENT '状态1=完成，2=待处理，0=关闭',
  `time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '订单创建时间',
  `updateTime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后更新时间',
  `bak` text COLLATE utf8mb4_general_ci COMMENT '备注',
  PRIMARY KEY (`orderId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

DROP TABLE IF EXISTS `pre_finance_recharge`;
CREATE TABLE `pre_finance_recharge` (
  `orderId` bigint NOT NULL COMMENT '充值单号',
  `uid` bigint NOT NULL DEFAULT '0' COMMENT '消费者',
  `balance` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'balance' COMMENT '充值的资产名称',
  `price` decimal(10,2) NOT NULL COMMENT '单价',
  `quantity` bigint NOT NULL,
  `amount` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '订单充值金额',
  `time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '订单时间',
  `payed` tinyint(1) DEFAULT '0' COMMENT '支付状态',
  `payedTime` datetime DEFAULT NULL COMMENT '支付时间',
  `payedAmount` decimal(20,2) DEFAULT '0.00' COMMENT '已付金额',
  `payment` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '支付方式',
  `cashierId` bigint DEFAULT '0' COMMENT '收银ID',
  `refund` tinyint(1) DEFAULT '0' COMMENT '是否发生退款',
  `refundTime` datetime DEFAULT NULL COMMENT '退款时间',
  `refundId` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '第三方退款记录ID',
  `refundAmount` decimal(20,2) DEFAULT '0.00' COMMENT '退款金额',
  PRIMARY KEY (`orderId`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='充值记录';

DROP TABLE IF EXISTS `pre_finance_refund`;
CREATE TABLE `pre_finance_refund` (
  `id` bigint NOT NULL COMMENT '退款号',
  `cashierId` bigint NOT NULL COMMENT '退款的收银单号',
  `amount` decimal(20,2) NOT NULL COMMENT '退款数额',
  `uid` bigint NOT NULL DEFAULT '0' COMMENT '对应UID',
  `editor` bigint NOT NULL DEFAULT '0' COMMENT '操作人',
  `state` tinyint(1) DEFAULT '2' COMMENT '状态',
  `time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '退款操作时间',
  `payment` varchar(30) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '退款方式',
  `refund` tinyint(1) DEFAULT '0' COMMENT '是否已退款成功',
  `outRefundId` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '第三方退款订单号',
  `refundTime` datetime DEFAULT NULL COMMENT '退款成功时间',
  `refundResult` text COLLATE utf8mb4_general_ci COMMENT '退款第三方返回数据',
  `refundAmount` decimal(20,2) DEFAULT '0.00' COMMENT '实际退款金额',
  `bak` text COLLATE utf8mb4_general_ci COMMENT '退款备注',
  `notify` int DEFAULT '0' COMMENT '发送通知的次数',
  `notified` int DEFAULT '0' COMMENT '接收到通知的次数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='收银退款（有进才有出）';

-- 2024.03.29 增加支持余额退款申请，具体退款方式，由管理员处理
CREATE TABLE `pre_finance_balanceRefund` (
    `id` bigint unsigned NOT NULL AUTO_INCREMENT,
    `uid` bigint NOT NULL,
    `name` varchar(30) COLLATE utf8mb4_general_ci DEFAULT 'balance',
    `amount` decimal(10,2) unsigned NOT NULL,
    `reason` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
    `state` tinyint(1) DEFAULT '2',
    `time` datetime DEFAULT CURRENT_TIMESTAMP,
    `bak` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
    `refundAmount` decimal(10,2) DEFAULT '0.00',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='余额退款申请';

-- 2025.02.22 修复支付金额，并保存优惠信息
ALTER TABLE `pre_finance_cashier` ADD `paymentBankType` varchar(50) default null COMMENT '成功支付的银行类型';
ALTER TABLE `pre_finance_cashier` ADD `promotionList` text default null COMMENT '支付时使用的优惠信息';