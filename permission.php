<?php
// +----------------------------------------------------------------------
// | INPHP
// | Copyright (c) 2023 https://inphp.cc All rights reserved.
// | Author: 幺月儿(https://gitee.com/lulanyin) Email: inphp@qq.com
// | 该文件的开源协议以所在项目的LICENSE文件为准，请遵守开源协议要求
// +----------------------------------------------------------------------
// | 权限配置项
// +----------------------------------------------------------------------
return [
    ["name" => "账户资产管理", "value" => "balance", "sub" => [
        ["name" => "余额查看", "value" => "balance"],
        ["name" => "流水查看", "value" => "history"],
        ["name" => "余额增减", "value" => "update"],
        ["name" => "兑换管理", "value" => "exchange"],
        ["name" => "转账管理", "value" => "transfer"],
        ["name" => "提现管理", "value" => "checkout"],
        ["name" => "退款管理", "value" => "refund"]
    ]],
    ["name" => "收银管理", "value" => "cashier", "sub" => [
        ["name" => "收银记录查看", "value" => "list"],
        ["name" => "退款记录查看", "value" => "refundList"],
        ["name" => "操作退款", "value" => "refund", "tooltip" => "操作后，仅仅生成退款单，还需要确认"],
        ["name" => "退款确认", "value" => "refundConfirm", "tooltip" => "确认后，款项才会原路退回，也可以拒绝退款"]
    ]],
    ["name" => "提现管理", "value" => "checkout", "sub" => [
        ["name" => "记录查看", "value" => "list"],
        ["name" => "提现操作确认", "value" => "confirm", "tooltip" => "确认后，提现单才结束，也可以拒绝提现，款项退回"]
    ]],
    ["name" => "查看充值记录", "value" => "recharge"],
    //["name" => "查看转账记录", "value" => "transfer"],
    //["name" => "查看兑换记录", "value" => "exchange"],
    ["name" => "参数配置", "value" => "config"]
];