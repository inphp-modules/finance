<?php
// +----------------------------------------------------------------------
// | INPHP
// | Copyright (c) https://inphp.cc All rights reserved.
// | 该文件源码由INPHP官方提供，使用协议以INPHP官方公告为准。
// +----------------------------------------------------------------------
// | 退款接口
// +----------------------------------------------------------------------
namespace app\finance\onlinePay;

use Inphp\Core\Object\Message;

interface IRefund
{
    //退款处理
    public function process(array $arg = []): Message;

    //退款结果异步退筹
    public function notify(?string $appId = null);
}