<?php
// +----------------------------------------------------------------------
// | INPHP
// | Copyright (c) https://inphp.cc All rights reserved.
// | 该文件源码由INPHP官方提供，使用协议以INPHP官方公告为准。
// +----------------------------------------------------------------------
// | 第三方支付接口
// +----------------------------------------------------------------------
namespace app\finance\onlinePay;

use Inphp\Core\Object\Message;

interface IPay
{
    //创建第三方支付订单
    public function prepay(array $arg = []): Message;

    //支付成功，同步跳转回的页面处理
    public function callback(?string $appId = null);

    //支付成功，异步通知
    public function notify(?string $appId = null);

}