<?php
// +----------------------------------------------------------------------
// | INPHP
// | Copyright (c) 2023 https://inphp.cc All rights reserved.
// | Author: 幺月儿(https://gitee.com/lulanyin) Email: inphp@qq.com
// | 该文件的开源协议以所在项目的LICENSE文件为准，请遵守开源协议要求
// +----------------------------------------------------------------------
// | 
// +----------------------------------------------------------------------
if(!function_exists('mergeArray2to1'))
{
    /**
     * 将二维数组合并成一维数据
     * 将子数组以替换式合并为一个数组
     * @param array $array
     * @return array
     */
    function mergeArray2to1(array $array): array
    {
        $data = [];
        foreach ($array as $item)
        {
            if(!empty($item) && is_array($item)) $data = array_merge($data, $item);
        }
        return $data;
    }
}

if(!function_exists('getFinanceConfig'))
{
    /**
     * 获取配置
     * @param string|null $map
     * @return mixed
     */
    function getFinanceConfig(string|null $map = null): mixed
    {
        return \Inphp\Core\Modules::getModule("finance")->getConfig($map);
    }
}

if(!function_exists('formatBalance'))
{
    /**
     * @param string $name
     * @param float|int $amount
     * @return float|int|string
     */
    function formatBalance(string $name, float|int $amount): float|int|string
    {
        $decimal = getFinanceConfig("balance.decimal");
        $decimal = is_array($decimal) ? $decimal : [];
        if(isset($decimal[$name]))
        {
            if($decimal[$name] > 0){
                $amount = sprintf("%.{$decimal[$name]}f", $amount);
            }else{
                $amount = floor($amount);
            }
        }
        return $amount;
    }
}