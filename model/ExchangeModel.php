<?php
// +----------------------------------------------------------------------
// | INPHP
// | Copyright (c) 2023 https://inphp.cc All rights reserved.
// | Author: 幺月儿(https://gitee.com/lulanyin) Email: inphp@qq.com
// | 该文件的开源协议以所在项目的LICENSE文件为准，请遵守开源协议要求
// +----------------------------------------------------------------------
// | 资产互兑
// +----------------------------------------------------------------------
namespace app\finance\model;

use Inphp\Core\Db\PDO\Model;

class ExchangeModel extends Model
{
    /**
     * 表名
     * @var string
     */
    protected string $tableName = "finance_exchange";

    /**
     * 主键
     * @var string
     */
    protected string $primaryKey = "orderId";
}