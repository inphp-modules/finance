<?php
// +----------------------------------------------------------------------
// | INPHP
// | Copyright (c) 2023 https://inphp.cc All rights reserved.
// | Author: 幺月儿(https://gitee.com/lulanyin) Email: inphp@qq.com
// | 该文件的开源协议以所在项目的LICENSE文件为准，请遵守开源协议要求
// +----------------------------------------------------------------------
// | 提现
// +----------------------------------------------------------------------
namespace app\finance\model;

use Inphp\Core\Db\PDO\Model;

class CheckoutModel extends Model
{
    /**
     * 表名
     * @var string
     */
    protected string $tableName = "finance_checkout";

    /**
     * 主键
     * @var string
     */
    protected string $primaryKey = "orderId";

    /**
     * 支付异步通知
     * 提现扣除资产 成功时
     * @param int $orderId
     * @param int $cashierId
     * @param float|int $payedQuantity
     */
    public static function payedNotify(int $orderId, int $cashierId, float|int $payedQuantity)
    {
        $time = date("Y-m-d H:i:s");
        $db = self::emptyQuery()
            ->where("orderId", $orderId)
            ->set("payedTime", $time)
            ->increment("payedQuantity", $payedQuantity)
            ->setRaw("cashierId", "if(`cashierId` is null or `cashierId`='', '{$cashierId}', concat(`cashierId`, ',{$cashierId}'))")
            ->setRaw("state", "if(`quantity`<=`payedQuantity` and `state`=2, 3, `state`)");
        if ($db->update() && $db->getAffectRows() > 0) {
            //标记为已通知
            CashierModel::payedNotified($cashierId);
        }
    }
}