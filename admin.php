<?php
// +----------------------------------------------------------------------
// | INPHP
// | Copyright (c) 2023 https://inphp.cc All rights reserved.
// | Author: 幺月儿(https://gitee.com/lulanyin) Email: inphp@qq.com
// | 该文件的开源协议以所在项目的LICENSE文件为准，请遵守开源协议要求
// +----------------------------------------------------------------------
// | 后台配置
// +----------------------------------------------------------------------
return [
    "menus" => [
        ["name" => "账户余额", "icon" => "fa-regular fa-database", "url" => "./admin/balance/list", "permission" => ["finance/balance/list"]],
        ["name" => "流水记录", "icon" => "fa-regular  fa-list-check", "url" => "./admin/balance/history", "permission" => ["finance/balance/history"]],
        ["name" => "收银记录", "icon" => "fa-regular  fa-yen-sign", "url" => "./admin/cashier/list", "permission" => ["finance/cashier/list"]],
        ["name" => "退款记录", "icon" => "fa-regular  fa-rotate-left", "url" => "./admin/cashier/refund-list", "permission" => ["finance/cashier/refund", "finance/cashier/refundList", "finance/cashier/refundConfirm"]],
        ["name" => "充值记录", "icon" => "fa-regular  fa-arrow-down", "url" => "./admin/recharge/list", "permission" => ["finance/recharge"]],
        //["name" => "发票记录", "icon" => "fa-light  fa-file-invoice", "url" => "./admin/invoice/list", "permission" => ["finance/invoice"]],
        ["name" => "余额退款", "icon" => "fa-regular  fa-rotate-left", "url" => "./admin/balance/refund", "permission" => ["finance/balance/refund"]],
        //["name" => "兑换记录", "icon" => "fa-regular  fa-arrows-to-dotted-line", "url" => "./admin/exchange/list", "permission" => ["finance/balance/exchange"]],
        //["name" => "转账记录", "icon" => "fa-regular  fa-exchange", "url" => "./admin/transfer/list", "permission" => ["finance/balance/transfer"]]
        /*
        ["name" => "配置", "icon" => "fa-regular  fa-gear", "permission" => ["finance/config"], "subMenu" => [
            ["name" => "资产类型", "icon" => "fa-regular  fa-database", "url" => "./admin/config/balance"],
            ["name" => "货币种类", "icon" => "fa-regular  fa-yen-sign", "url" => "./admin/config/currency"],
            ["name" => "订单类型", "icon" => "fa-regular  fa-list", "url" => "./admin/config/order-type"],
            ["name" => "在线支付", "icon" => "fa-brands  fa-alipay", "url" => "./admin/config/online-pay"],
            ["name" => "其它支付", "icon" => "fa-regular  fa-question", "url" => "./admin/config/other-pay"],
        ]],*/
    ]
];