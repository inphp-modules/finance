<?php
// +----------------------------------------------------------------------
// | INPHP
// | Copyright (c) 2023 https://inphp.cc All rights reserved.
// | Author: 幺月儿(https://gitee.com/lulanyin) Email: inphp@qq.com
// | 该文件的开源协议以所在项目的LICENSE文件为准，请遵守开源协议要求
// +----------------------------------------------------------------------
// | 业务配置
// +----------------------------------------------------------------------
return [
    //资产
    "balance"       => [
        "list"          => [
            "balance"       => "余额",
            "integral"      => "积分"
        ],
        "decimal"       => [
            "balance"       => 2,
            "integral"      => 0
        ]
    ],
    //货币名称，第一个是法定货币，所有资产对比都是与第一个对
    "currency"      => [
        "CNY"       => [
            //名称
            "name"  => "人民币",
            //单位
            "unit"  => "元"
        ],
        "USD"       => [
            "name"  => "美元",
            "unit"  => "美元"
        ]
    ],
    //流水记录
    "history"   => [
        //是否开启签名校验，开启后虽然更安全，但性能会消耗更多
        "sign"      => true,
        //签名加密字符
        "signKey"   => "1x2y3z",
        //流水事件代码解析，可以是数组，也可以是绝对文件路径(推荐)
        "codes"    => [
            "admin"     => "管理员操作",
            "recharge"  => "充值"
        ]
    ],
    //收银
    "cashier"       => [
        //订单种类
        "orderTypes"    => [
            //key 一般使用表名
            "finance_checkout"      => "提现订单",
            "finance_transfer"      => "转账订单",
            "finance_recharge"      => "充值订单",
        ],
        //支持使用账户资产（余额、积分等）进行抵扣支付的订单类型（下方参数值仅作示例，请根据自己的项目进行调整和增减订单类型）且需要前端配合使用
        "balancePayOrderTypes"  => [
            //商城订单允许
            "mall_order"            => [
                //是否必须全额支付
                "fullAmount"            => false,
                //可用资产
                "balances"              => [
                    "balance"               => [
                        //抵扣的汇率
                        "ratio"                 => 1,
                        //最小可使用的数额
                        "minAmount"             => 0.01,
                        //基数
                        "multiBase"             => 0.01,
                        //最大抵扣比例
                        "maxPercent"            => 1
                    ],
                    "integral"              => [
                        //抵扣的汇率
                        "ratio"                 => 0.01,
                        //最小可使用的数额
                        "minAmount"             => 100,
                        //基数
                        "multiBase"             => 100,
                        //最大抵扣比例
                        "maxPercent"            => 0.5
                    ]
                ]
            ]
        ],
        //支持支付接口创建的订单类型
        "generateOrderTypes"    => [
            //填写订单表名
            "finance_recharge"
        ],
        //需要使用中间键方式获取的订单类型
        "outsideOrderTypes"     => [

        ],
        //在线支付方式
        "onlinePayments"    => [
            //微信
            "wechat"        => [
                //名称
                "name"      => "微信",
                //是否开通该支付通道
                "enable"    => true,
                //网页版支付地址，后尾会自动添加 id=收银单据ID
                "h5Url"     => "/finance/pay/index",
                //appid
                "appList"   => [
                    //key 为APPID
                    "应用APPID"    => [
                        //
                        "appId"     => "应用APPID",
                        //app secret
                        "secret"    => "应用APP密钥",
                        //商户ID
                        "mchId"     => "微信商户ID",
                        //API V3 密钥
                        "apiKey"    => "",
                        //证书内容
                        "cert"      => "",
                        //证书SN
                        "certSN"    => "",
                        //私钥
                        "key"       => "",
                        //提交到微信支付时的订单号附加前缀
                        "outTradeNoPrefix" => ""
                    ]
                ]
            ],
            //支付宝
            "alipay"    => [
                //名称
                "name"      => "支付宝",
                //是否开通该支付通道
                "enable"    => true,
                //网页版支付地址，后尾会自动添加 id=收银单据ID
                "h5Url"     => "/finance/pay/index",
                //appid
                "appList"   => [
                    //key: 应用ID
                    "应用APPID" => [
                        //应用ID
                        "appId"             => "",
                        //接口网关
                        "gatewayUrl"        => "https://openapi.alipay.com/gateway.do",
                        //私钥，请使用RSA2加密方式，在支付宝工具中取得
                        "privateKey"        => "",
                        //支付宝公钥，在应用配置网页中取得
                        "publicKey"         => "",
                        //提交到支付时的订单号附加前缀
                        "outTradeNoPrefix"  => ""
                    ]
                ]
            ],
            //其余自行配置
        ],
        //其它支付方式，仅仅是为了显示名称
        "otherPayments" => [
            //key => 名称
            "localWechat"   => "微信扫码付款",
            "localAlipay"   => "支付宝扫码付款",
            "cash"          => "现金付款",
            "bank"          => "银行转账"
        ]
    ],
    //退款
    "refund"    => [
        //是否需要二次确认？
        "needConfirm"   => true
    ],
    //提现，仅支持银行卡、微信、支付宝提现
    "checkout"  => [
        //可提现资产类型
        "balance"   => [
            "balance"   => [
                //与法定货币汇率
                "ratio" => 1,
                //手续费
                "fee"   => 0.05,
                //手续费可划入哪个资产
                "feeTo" => null,
                //手续费划入资产的汇率
                "feeToRatio" => 0,
                //最小提现金额
                "min"   => 100,
                //最大提现金额
                "max"   => 5000
            ]
        ],
        //是否需要操作确认
        "needConfirm"   => true
    ],
    //可兑换
    "exchange"  => [
        //资产
        "integral" => [
            //可兑换为
            "balance" => [
                //手续费
                "fee"   => 0,
                //比例
                "multi" => 0.01,
                //最小兑换数量
                "min"   => 100
            ]
        ]
    ],
    //可转账资产
    "transfer" => [
        //资产
        "integral"  => [
            //手续费
            "fee"   => 0,
            //最小转账数量
            "min"   => 100
        ],
        "balance"  => [
            //手续费
            "fee"   => 0,
            //最小转账数量
            "min"   => 10
        ]
    ],
    "recharge"  => [
        "integral"  => [
            //状态
            "state"     => false,
            //单价
            "price"     => 0.01,
            //最小充值数量
            "min"       => 10000,
            //最大充值量
            "max"       => 1000000
        ],
        "balance"  => [
            //状态
            "state"     => true,
            //单价
            "price"     => 1,
            //最小充值数量
            "min"       => 5,
            //最大充值量
            "max"       => 1000000
        ]
    ]
];