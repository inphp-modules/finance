<?php
// +----------------------------------------------------------------------
// | INPHP
// | Copyright (c) 2023 https://inphp.cc All rights reserved.
// | Author: 幺月儿(https://gitee.com/lulanyin) Email: inphp@qq.com
// | 该文件的开源协议以所在项目的LICENSE文件为准，请遵守开源协议要求
// +----------------------------------------------------------------------
// | 收银模块中间键
// +----------------------------------------------------------------------

// 支付成功通知
const FINANCE_PAYED_NOTIFY = "financePayedNotify";
// 退款在无法处理时，会使该通知
const FINANCE_REFUND = "financeRefund";
// 退款成功通知
const FINANCE_REFUND_NOTIFY = "financeRefundNotify";
// 获取外部订单数据
const FINANCE_GET_OUTSIDE_ORDER = "financeGetOutsideOrder";
// 在线支付无法处理时，会使用该通知
const FINANCE_ONLINE_PAY = "financeOnlinePay";
// 提现完成(已被确认，关闭也算)
const FINANCE_CHECKOUT = "financeCheckout";
// 兑换完成
const FINANCE_EXCHANGE = "financeExchange";
// 转账完成
const FINANCE_TRANSFER = "financeTransfer";

//处理支付
\Inphp\Core\Middlewares::push(FINANCE_ONLINE_PAY, function (...$args) {
    //支付名称
    $name = $args["name"] ?? ($args["payment"] ?? null);
    //支付插件检测
    $class = join("\\", ['app\finance\onlinePay', $name, 'pay']);
    if (!class_exists($class)) {
        //不存在
        return [
            "error"     => 1,
            "message"   => "不存在的支付方式：{$name}"
        ];
    }
    //检测创建订单方法是否存在
    if (!method_exists($class, "prepay")) {
        //不存在
        return [
            "error"     => 1,
            "message"   => "该支付方式：{$name}，不存在相应的处理方法：prepay"
        ];
    }
    //执行对应的支付接口，生成支付平台的支付订单
    $obj = new $class();
    $result = $obj->prepay($args);
    if ($result instanceof \Inphp\Core\Object\Message) {
        return $result->toArray();
    }
    return is_array($result) ? $result : [
        "error"     => 1,
        "message"   => "未知的处理结果"
    ];
});

//处理退款
\Inphp\Core\Middlewares::push(FINANCE_REFUND, function (...$args) {
    $name = $args["name"] ?? ($args["payment"] ?? null);
    //支付插件检测
    $onlinePayments = \app\finance\model\CashierModel::onlinePayments();
    $onlinePayments = !empty($onlinePayments) ? array_keys($onlinePayments) : [];
    if (in_array($name, $onlinePayments)) {
        $class = join("\\", ['app\finance\onlinePay', $name, 'refund']);
        if (!class_exists($class)) {
            //不存在
            return [
                "error"     => 1,
                "message"   => "不存在的支付方式：{$name}"
            ];
        }
        //检测创建订单方法是否存在
        if (!method_exists($class, "process")) {
            //不存在
            return [
                "error"     => 1,
                "message"   => "该支付方式：{$name}，不存在相应的处理方法：process"
            ];
        }
        //执行对应的支付接口，生成支付平台的支付订单
        $obj = new $class();
        $result = $obj->process($args);
        if ($result instanceof \Inphp\Core\Object\Message) {
            return $result->toArray();
        }
        return is_array($result) ? $result : [
            "error"     => 1,
            "message"   => "未知的处理结果"
        ];
    }
    //其它支付方式
    return [
        "error"     => 1,
        "message"   => "无对应支付方式退款处理"
    ];
});

//支付完成
\Inphp\Core\Middlewares::push(FINANCE_PAYED_NOTIFY, function (...$args) {
    $date = date("Ymd");
    \Inphp\Core\Util\Log::writeToEnd(RUNTIME."/logs/finance/wechat/pay/{$date}.txt", $date." [支付成功中间键]\r\n".json_encode($args, 256));
    switch ($args["orderType"]) {
        //提现扣款成功，通知订单更新状态
        case "finance_checkout":
            \app\finance\model\CheckoutModel::payedNotify($args["orderId"], $args["cashierId"], $args["payedAmount"]);
            break;
        //充值成功，通知订单更新状态
        case "finance_recharge":
            \app\finance\model\RechargeModel::payedNotify($args["orderId"], $args["cashierId"], $args["payedAmount"]);
            break;
    }
});

//退款异步通知
\Inphp\Core\Middlewares::push(FINANCE_REFUND_NOTIFY, function (...$args) {
    switch ($args["orderType"]) {
        //充值退款
        case "finance_recharge":
            \app\finance\model\RechargeModel::refundNotify($args["orderId"], $args["refundId"], $args["refundAmount"]);
            break;
    }
});